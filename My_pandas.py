__author__ = 'uidh2520'
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import os
print "******************** 1D example ******************"
'Series is a one-dimensional labeled array capable of holding data of any type (integer, string, float, python objects, etc.)' \
'The axis labels are collectively called index.'
#it can be used as excel and sql table like hetrogenous -typed coloumn.
#it is used as analysing data , cleaning data
#syntax = pd.Series(data=d,index=[])
s1= pd.Series([11,21,31])
s2= pd.Series(list('AbcAd'))
s3=pd.Series(["Ram","Mohan","shyam"],index = [10,20,30])
s4 =pd.Series(range(5),index=list('abcda'))
print s1 ,"\n",s2 ,"\n",s3,"\n",s4
# access the panda data
print s4['a'] #o/p - a=0,a=4
print s4['a'][0]
print "**********************************8 dictioanlry example"
my_dict = {'a':100,'b':200}
s5 = pd.Series(my_dict)
print "\n",s5

s6 = pd.Series(my_dict,index=list('abc'))
print "\n",s6
print "\n",s6*2
print "******************** 2D example ******************"
d={'one':pd.Series([1,2,3],index=['a','b','c']),
   'two':pd.Series([1,2,3,4],index=['a','b','c','d'])}
df=pd.DataFrame(d)
print df
print "\n",df['one']
df['three']=pd.Series([100,200,300,400],index=['a','b','c','d'])
print "\n",df

names = ['Bob','Jessica','Mary','John','Mel']
births = [968, 155, 77, 578, 973]
BabyDataSet = list(zip(names,births))
df = pd.DataFrame(data=BabyDataSet,columns=['Name','Births'])
print df
df.to_csv('births1880.csv',index=False,header=False)
path=(os.getcwd()).replace("\\","/")+"\\"+"births1880.csv"

print path
#df = pd.read_csv(path)
df = pd.read_csv(path, names=['Names','Births'])
print df

# create a graph
df['Births'].plot()
# get the maximum value  in birth
MaxValue = df['Births'].max()
# get the name of maximum value in birth
MaxName = df['Names'][df['Births']==df['Births'].max()].values
#txt to display to graph
Text = str(MaxValue) + " - " + MaxName
# add txt to graph
plt.annotate(Text, xy=(1, MaxValue), xytext=(8, 0),
                 xycoords=('axes fraction', 'data'), textcoords='offset points')
print ("The most popular names")
plt.show()



o/p-C:\Python27\python.exe C:/Users/uidh2520/PycharmProjects/OOps/.idea/scopes/My_pandas.py
******************** 1D example ******************
0    11
1    21
2    31
dtype: int64 
0    A
1    b
2    c
3    A
4    d
dtype: object 
10      Ram
20    Mohan
30    shyam
dtype: object 
a    0
b    1
c    2
d    3
a    4
dtype: int64
a    0
a    4
dtype: int64
0
**********************************8 dictioanlry example

a    100
b    200
dtype: int64

a    100
b    200
c    NaN
dtype: float64

a    200
b    400
c    NaN
dtype: float64
******************** 2D example ******************
   one  two
a    1    1
b    2    2
c    3    3
d  NaN    4

a     1
b     2
c     3
d   NaN
Name: one, dtype: float64

   one  two  three
a    1    1    100
b    2    2    200
c    3    3    300
d  NaN    4    400
      Name  Births
0      Bob     968
1  Jessica     155
2     Mary      77
3     John     578
4      Mel     973
C:/Users/uidh2520/PycharmProjects/OOps/.idea/scopes\births1880.csv
     Names  Births
0      Bob     968
1  Jessica     155
2     Mary      77
3     John     578
4      Mel     973
973
['Mel']
The most popular names

Process finished with exit code 0
