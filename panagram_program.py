# '''
# Given a string check if it is Pangram or not. A pangram is a sentence containing every letter in the English Alphabet.
#
# Examples : The quick brown fox jumps over the lazy dog ” is a Pangram [Contains all the characters from ‘a’ to ‘z’]
# “The quick brown fox jumps over the lazy dog” is not a Pangram [Doesn’t contains all the characters from ‘a’ to ‘z’, as ‘l’, ‘z’, ‘y’ are missing]
# '''

def panagram_func_ascii(string):
    bool_check = []
    for i in range(26):
        bool_check.append(False)
    for character in string.lower():
        if not character == " ":
            bool_check[ord(character)-ord('a')] = True
    for boolean in bool_check:
        if boolean != True:
            return False
    return True

def panagram_func_list_comprehension(string):

    pangaram_data = "abcdefghijklmnopqrstuvwxyz"
    not_present = [ alphabet for alphabet in pangaram_data if alphabet not in string.lower()]
    if len(not_present) > 0:
        print("The charcter which is missing : ",not_present)
    #print(len( [alphabet for alphabet in pangaram_data if alphabet not in string.lower()] ) == 0)
    return len( [alphabet for alphabet in pangaram_data if alphabet not in string.lower()] ) == 0

def panagram_func_by_dict(string):
    # in this pg u dnt need  to take "alphabetical string
    my_dict = {}
    for character in string.lower():
        if character in my_dict:
            my_dict[character] +=1
        else:
            my_dict[character] = 1
    if (len(my_dict.keys())-1 == 26 ):
        print("dict ways : panagram")
    else:
        print("dict way : not a panagram")


def panagram_func(string):
    pangaram_data = "abcdefghijklmnopqrstuvwxyz"
    length = len(pangaram_data)
    for character in string:
        character = character.lower()
        if character in pangaram_data:
            index_1 = pangaram_data.index(character)
            pre_str = pangaram_data[0:index_1]
            post_str = pangaram_data[index_1+1:]
            pangaram_data = pre_str + post_str
    if len(pangaram_data)>0:
        print(string + " is not panagram string ")
        print(" The letter which is missing ",pangaram_data)
    else:
        print(string+" is a palangram program")

#check import works , if u import one module so many times what will happen
if __name__ == '__main__':
    string = "The quick brown fox jumps over the lazy dog"
    panagram_func(string)
    panagram_func_by_dict(string)
    if panagram_func_list_comprehension(string):
        print("list comprehension : panagram")
    else:
        print("list com[rehension : not panagram")
    if (panagram_func_ascii(string)):
        print("By ASCii : string is pangram")
    else:
        print("By Ascii : string is not pangram")
