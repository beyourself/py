# Given list

lst = [ 1, 2, 3, 4 , 5]

# we want square of each element of given list

result = []
for num in lst:
    result.append(num * num)

print result


#using map

def square(x):
    return x * x


result = map(square, lst)
print result


# using map and lambda


result = map(lambda x: x * x, lst)

print result


# Given a list of words and you need to find length of words

words_lst = "hi i am jay kant kumar".split()

# obvious way
length_lst = []
for word in words_lst:
    length_lst.append(len(word))

print length_lst

# map way using function
def length(word):
    return len(word)

length_lst = map(length, words_lst)

print length_lst

# use map and lambda
length_lst = map(lambda word: len(word), words_lst)

print length_lst

# use len library method
length_lst = map(len, words_lst)
print length_lst



## filter


lst = [-5, -4, -3, -2, -1, 0, 1, 2, 3, 4]


# obvious way to implement "get list of all numbers which are smaller than 0"
result = []
for num in lst:
    if num < 0 :
        result.append(num)


# using function
def check_less_than_zero(num):
    return num < 0


result = filter(check_less_than_zero, lst)

# using lambda and filter

result = filter(lambda num : num < 0 , lst)


## reduce

from functools import reduce

lst = [1, 2, 3, 4]

result = 1

for num in lst:
    result = result * num
    print result

print 'so final value is ' , result

reduce( (lambda x, y: x * y), [1, 2, 3, 4] )