'Author : Preety kumari'
'Date : 29th May 2018'

class Lambda:

    # constructor
    def __init__(self,x,y):
        self.x = x
        self.y = y
        self.f = None
    #method
    def create_lambda_function(self):
        x=self.x
        y=self.y
        self.f = lambda x , y: x + y
        return self.f

if __name__ == '__main__':
    lambdaObj = Lambda(1,2)
    print lambdaObj.create_lambda_function()