__author__ = 'uidh2520'
'Author : Preety Kumari'
'Date : 20 May 2018 '

def get_max_number(func): # here func is function name which is called from main. eg : -find_frequent_letter ,find_frequent_word_using_dictonary
    def wrapper(*args): # args is argument or parameter which is passed to function , eg - file_path which is passed to find_frequent_letter
        my_dict =func(*args)
        key_max =max(my_dict.keys(), key=(lambda k: my_dict[k]))
        print (func.__name__+ "has max number occurence of  " , key_max)
        return key_max
    return wrapper

# decorater used
@get_max_number
def find_frequent_word_using_dictonary(file_path):
    my_dict = {}
    my_data_list =None
    counter=1
    with open(file_path) as fr:
        while True:
            char =fr.read()
            if not char:break
            for word in char.split(" "):
                if word in my_dict:
                    my_dict[word]+=1
                    #counter=my_dict[word]+1
                    #my_dict[word]=counter

                else:
                    my_dict[word]=1
    return my_dict


@get_max_number
def find_frequent_letter_using_dictonary(file_path):
    my_dict ={}
    counter =1
    i=0
    with open(file_path) as fr:
        data = fr.readline()
        data_len = len(data)
        while(data_len!=0):
            if data[i] in my_dict:
                my_dict[data[i]]+=1
                #counter=my_dict.get(data[i])
                #counter+=1
                #my_dict[data[i]]=counter
            else:
                my_dict[data[i]]=1
            data_len-=1
            i+=1
    return my_dict
@get_max_number
def find_frequent_letter(file_path):
    my_dict ={}
    fr = open(file_path)
    data_file = fr.readline()
    for letter in range(len(data_file)-1):
        count =0
        j=letter+1
        for j in range(len(data_file)):
            if (data_file[letter]== data_file[j]):
                count =count+1
                my_dict[data_file[j]]=count
    return my_dict

    #[for key in my_dict if max <= my_dict.get(key) : maxkey = key, max = my_dict.get(key) ]

    #key_max =max(my_dict.keys(), key=(lambda k: my_dict[k]))
    #print key_max , my_dict[key_max]


if __name__ == '__main__':
    file_path ="D:\Preety_D_ADAS_data\my_file.txt"
    file_path=file_path.replace("\\",'/')
    print(find_frequent_letter(file_path))
    print(find_frequent_letter_using_dictonary(file_path))
    print(find_frequent_word_using_dictonary(file_path))
    
o/p-
('find_frequent_letter:', 'P')
P
('find_frequent_letter_using_dictonary:', 'P')
P
('find_frequent_word_using_dictonary:', 'Preety')
Preety