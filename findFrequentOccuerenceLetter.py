__author__ = 'uidh2520'
'Author : Preety Kumari'
'Date : 20 May 2018 '
'question : find the frequent letter or word in file'

import sys
def find_frequent_word_using_dictonary(file_path):
    my_dict = {}
    my_data_list =None
    with open(file_path) as fr:
        while True:
            char =fr.read()
            print char
            if not char:break
            for word in char.split(" "):
                if word in my_dict:
                    my_dict[word]+=1
                else:
                    my_dict[word]=1
    print my_dict
    key_max =max(my_dict.keys(), key=(lambda k: my_dict[k]))
    print key_max

def find_frequent_letter_using_dictonary(file_path):
    my_dict ={}
    i=0
    with open(file_path) as fr:
        data = fr.readline()
        data_len = len(data)
        while(data_len!=0):
            if data[i] in my_dict:
                my_dict[data[i]]+=1
            else:
                my_dict[data[i]]=1
            data_len-=1
            i+=1
        print my_dict
    max1=0
    key1=None
    for i in (my_dict):
        if max1 <= my_dict.get(i):
            max1=my_dict.get(i)
            key1= i
    print max1,key1

def find_frequent_letter(file_path):
    my_dict ={}
    fr = open(file_path)
    data_file = fr.readline()
    for letter in range(len(data_file)-1):
        count =0
        j=letter+1
        for j in range(len(data_file)):
            if (data_file[letter]== data_file[j]):
                count =count+1
                my_dict[data_file[j]]=count
    print my_dict
    max=0
    maxkey =None
    for key in my_dict:
        if max <= my_dict.get(key):
            max = my_dict.get(key)
            maxkey=key
    print max ,maxkey

    #[for key in my_dict if max <= my_dict.get(key) : maxkey = key, max = my_dict.get(key) ]

    #key_max =max(my_dict.keys(), key=(lambda k: my_dict[k]))
    #print key_max , my_dict[key_max]


if __name__ == '__main__':
    file_path ="D:\Preety_D_ADAS_data\my_file.txt"
    file_path=file_path.replace("\\",'/')
    find_frequent_letter(file_path)
    find_frequent_letter_using_dictonary(file_path)
    find_frequent_word_using_dictonary(file_path)
    
    
o/p -{'a': 4, ' ': 6, 'b': 1, 'e': 6, 'g': 1, 'i': 2, 'k': 1, 'm': 3, 'l': 1, 'o': 1, 'n': 2, 'P': 2, 's': 1, 'r': 4, 'u': 1, 't': 2, 'y': 3}
6 e
{'a': 4, ' ': 6, 'b': 1, 'e': 6, 'g': 1, 'i': 2, 'k': 1, '\n': 1, 'm': 3, 'l': 1, 'o': 1, 'n': 2, 'P': 2, 's': 1, 'r': 4, 'u': 1, 't': 2, 'y': 3}
6 e
Preety is my name Preety kumari bangalore


{'kumari': 1, 'name': 1, 'is': 1, 'Preety': 2, 'bangalore\n': 1, 'my': 1}
Preety